const {
  KNIGHT,
  QUEEN,
  ROOK,
  BISHOP,
  KING,
  PAWN,
  BLACK,
  WHITE
} = require('../constants.js');

const knight_black_img = require('./images/knight.png');
const queen_black_img = require('./images/queen.png');
const king_black_img = require('./images/king.png');
const rook_black_img = require('./images/rook.png');
const bishop_black_img = require('./images/bishop.png');
const pawn_black_img = require('./images/pawn.png');

const knight_white_img = require('./images/knight_white.png');
const queen_white_img = require('./images/queen_white.png');
const king_white_img = require('./images/king_white.png');
const rook_white_img = require('./images/rook_white.png');
const bishop_white_img = require('./images/bishop_white.png');
const pawn_white_img = require('./images/pawn_white.png');

let queen_black =  {
  img: queen_black_img,
  color: BLACK,
  type: QUEEN
}

let knight_black =  {
  img: knight_black_img,
  color: BLACK,
  type: KNIGHT
}

let king_black =  {
  img: king_black_img,
  color: BLACK,
  type: KING
}

let bishop_black =  {
  img: bishop_black_img,
  color: BLACK,
  type: BISHOP
}

let rook_black =  {
  img: rook_black_img,
  color: BLACK,
  type: ROOK
}

let pawn_black =  {
  img: pawn_black_img,
  color: BLACK,
  type: PAWN,
  yDir: -1
}

let queen_white =  {
  img: queen_white_img,
  color: WHITE,
  type: QUEEN
}

let knight_white =  {
  img: knight_white_img,
  color: WHITE,
  type: KNIGHT
}

let king_white =  {
  img: king_white_img,
  color: WHITE,
  type: KING
}

let bishop_white =  {
  img: bishop_white_img,
  color: WHITE,
  type: BISHOP
}

let rook_white =  {
  img: rook_white_img,
  color: WHITE,
  type: ROOK
}

let pawn_white =  {
  img: pawn_white_img,
  color: WHITE,
  type: PAWN,
  yDir: 1
}

exports.queen_black = queen_black;
exports.knight_black = knight_black;
exports.king_black = king_black;
exports.bishop_black = bishop_black;
exports.rook_black = rook_black;
exports.pawn_black = pawn_black;

exports.queen_white = queen_white;
exports.knight_white = knight_white;
exports.king_white = king_white;
exports.bishop_white = bishop_white;
exports.rook_white = rook_white;
exports.pawn_white = pawn_white;
