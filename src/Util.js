let deepCopyArray = (tiles) => {
  return JSON.parse(JSON.stringify(tiles));
}

exports.deepCopyArray = deepCopyArray;
