import tiles from './resources/chessTiles.js';
import {
  KNIGHT,
  QUEEN,
  ROOK,
  BISHOP,
  KING,
  PAWN,
  BLACK,
  WHITE,
  RED,
  SELECT_TILE,
  SELECT_GAME_TYPE
} from './resources/constants.js';

const initialState = {
  tiles: tiles,
  selectedTile: null,
  activeColor: WHITE,
  captured: [],
  turnNbr: 0,
  gameType: null,
  selectedColor: null
}

function chessBoardReducer(state = initialState, action) {
  switch (action.type){
    case SELECT_TILE:
      return {
        tiles: action.tiles,
        selectedTile: action.selectedTile,
        activeColor: action.activeColor,
        captured: action.captured,
        turnNbr: action.turnNbr,
        gameType: state.gameType,
        selectedColor: state.selectedColor,
        moveIsPending: action.moveIsPending,
        cpuColor: state.cpuColor,
        isCpuTurnActive: state.cpuColor == action.activeColor,
      }
    case SELECT_GAME_TYPE:
      return {
        tiles: initialState.tiles,
        selectedTile: initialState.selectedTile,
        activeColor: initialState.activeColor,
        captured: initialState.captured,
        turnNbr: initialState.turnNbr,
        gameType: action.gameType,
        selectedColor: action.selectedColor,
        cpuColor: action.cpuColor,
        isCpuTurnActive: action.cpuColor == initialState.activeColor
      }
    default:
      return state
  }
}

const reducer = {
  chessBoardReducer
}

export default reducer;
