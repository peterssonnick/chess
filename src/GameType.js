import React, {Component} from 'react';
import { Card, CardImg, CardText, CardBody,
  CardTitle, Button, CardGroup } from 'reactstrap';

class GameType extends Component {

  render() {
    return (
      <span
        style={{
          display: 'inlineBlock',
          width: '50px'
        }}
      >
        <img
          src={this.props.playerImg}
          alt={`${this.props.color != null ? this.props.color : this.props.gameType} Queen`}
          width={'50px'}
        />
        <Button
          onClick={()=> { this.props.onSelectGameType(this.props.gameType, this.props.color) }}
        >
          {this.props.color != null
            ? `Play as ${this.props.color}`
            : `Play ${this.props.gameType} game.`
          }
        </Button>
      </span>
    )
  }
}

export default GameType;
