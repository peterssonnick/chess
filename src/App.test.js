import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import ChessBoard from './ChessBoard';
import ChessTile from './ChessTile';
import {
  KNIGHT,
  QUEEN,
  ROOK,
  BISHOP,
  KING,
  PAWN,
  BLACK,
  WHITE,
  RED
} from './resources/constants.js';
import { shallow, mount } from 'enzyme';

describe('App', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(
      <App />
    );
  });

  it('This will be true', () => {
    expect(true).toBe(true);
  });

  it('should have a <ChessBoard />', () => {
    expect(
      wrapper.containsMatchingElement(
        <ChessBoard />
      )
    ).toBe(true);
  });

  it('should have a <ChessTile />', () => {
    expect(
      wrapper.containsMatchingElement(
        <ChessTile />
      )
    ).toBe(true);
  });

  describe('For the first tile on the board', () => {
    beforeEach(() => {
      const tile = wrapper
        .find('ChessTile').first();
      tile.simulate('click');
    });

    it('First ChessTile should be a Rook', () => {
      const tile = wrapper
        .find('ChessTile').first();

      expect(
        tile.props().occupant.type == ROOK
      ).toBe(true);
    });

    it('Rook should be White', () => {
      const tile = wrapper
        .find('ChessTile').first();

      expect(
        tile.props().occupant.color
      ).toEqual(WHITE);
    });
  });

  describe('For the KNIGHT...', () => {
    beforeEach(() => {
      const knight = wrapper
        .find('ChessTile')
        .find('[row=1]')
        .find('[column=2]')
        .first();
    });

    it('Knight should be loaded in row 1, col 2', () => {
      const knight = wrapper
        .find('[row=1]')
        .find('[column=2]')
        .first();

      expect(
        knight.props().occupant.type == KNIGHT
      ).toBe(true);
    });
  });
});
