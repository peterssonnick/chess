import React, { Component } from 'react';
import {
  KNIGHT,
  QUEEN,
  ROOK,
  BISHOP,
  KING,
  PAWN,
  BLACK,
  WHITE,
  RED
} from './resources/constants.js';

const players = require('./resources/pieces/players.js');
const occupants = [players.pawn_white, players.pawn_white, players.pawn_white, players.pawn_white, players.pawn_black];

class Captured extends Component {

  state = this.props.store.getState();

  componentDidMount() {
    this.props.store.subscribe(() => this.forceUpdate());
  }

  render() {
    return(
      <div
        style={{marginTop: '5px'}}
      >
        <WhiteCaptured

          occupants={this.props.store.getState().captured.filter(
            (occupant) => { return occupant.color === WHITE }
          )}
        />
        <BlackCaptured
          occupants={this.props.store.getState().captured.filter(
            (occupant) => { return occupant.color === BLACK }
          )}
        />
      </div>
    )
  }
}

class WhiteCaptured extends Component {
  render() {
    return (
      <span
        style={{
          outline: '1px solid black'
        }}
      >
        {this.props.occupants.map((occupant) => {
          return (
            <img
              key={occupant.key}
              src={occupant.img}
              style={{
                width: '30px',
                height: '30px'
              }}
            />
          )
        })}
      </span>
    );
  }
}

class BlackCaptured extends Component {
  render() {
    return (
      <span
        style={{
          outline: '1px solid black'
        }}
      >
        {this.props.occupants.map((occupant) => {
          return (
            <img
              key={occupant.key}
              src={occupant.img}
              style={{
                width: '30px',
                height: '30px'
              }}
            />
          )
        })}
      </span>
    );
  }
}

export default Captured;
