import {
  KNIGHT,
  QUEEN,
  ROOK,
  BISHOP,
  KING,
  PAWN,
  BLACK,
  WHITE,
  RED
} from './resources/constants.js';

let getTilesInRow = (selectedTile, tiles) => {
  return tiles.filter((tile) => {
    return tile.row === selectedTile.row
  });
}

let getTilesInColumn = (selectedTile, tiles) => {
  return tiles.filter((tile) => {
    return tile.column === selectedTile.column
  });
}

let getTilesInDiag = (selectedTile, tiles) => {
  return tiles.filter((tile) => {
    return Math.abs(tile.column - selectedTile.column) === Math.abs(tile.row - selectedTile.row);
  })
}

let getTilesInKnightJump = (selectedTile, tiles) => {
  let row = selectedTile.row;
  let column = selectedTile.column;

  return tiles.filter((tile) => {
    if (tile.row === row + 2 && tile.column === column + 1) {
      return tile;
    } else if (tile.row === row + 2 && tile.column === column - 1) {
      return tile;
    } else if (tile.row === row + 1 && tile.column === column + 2) {
      return tile;
    } else if (tile.row === row + 1 && tile.column === column - 2) {
      return tile;
    } else if (tile.row === row - 2 && tile.column === column + 1) {
      return tile;
    } else if (tile.row === row - 2 && tile.column === column - 1) {
      return tile;
    } else if (tile.row === row - 1 && tile.column === column + 2) {
      return tile;
    } else if (tile.row === row - 1 && tile.column === column - 2) {
      return tile;
    }
  })
}

let findMyKing = (tiles, color) => {
  return tiles.find((tile) => {
    return tile.occupant && tile.occupant.type === KING && tile.occupant.color == color;
  });
}


const TileFinders = {
  getTilesInRow,
  getTilesInColumn,
  getTilesInDiag,
  getTilesInKnightJump,
  findMyKing
}

export default TileFinders;
