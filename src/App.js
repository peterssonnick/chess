import React, { Component } from 'react';
import ChessBoard from './ChessBoard';
import GameType from './GameType';
import Captured from './Captured';
import { createStore } from 'redux';
import reducer from './ChessBoardReducer.js'
import {CardGroup} from 'reactstrap'
import './App.css';

const store = createStore(reducer.chessBoardReducer);
const queen_black_img = require('./resources/pieces/images/queen.png');
const queen_white_img = require('./resources/pieces/images/queen_white.png');
const cpu_img = require('./resources/pieces/images/cpu.jpg');

const {
  BLACK,
  WHITE,
  MULTI
} = require('./resources/constants.js');

class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
      'gameType': null,
      'selectedColor': null
    }
  }

  componentDidMount = () => {
    this.setState({
      'gameType': null,
      'selectedColor': null
    })
  }

  onSelectGameType = (gameType, selectedColor) => {
    this.setState({
      'gameType': gameType,
      'selectedColor': selectedColor
    })
  }

  render() {
    if (this.state.gameType != null) {
      return (
        <div className="App">
          <ChessBoard
            gameType={this.state.gameType}
            selectedColor={this.state.selectedColor}
            store={store}
          />
          <Captured
            store={store}
          />
        </div>
      );
    } else {
      return (
        <div>
          <GameType
            gameType={'Single'}
            color={WHITE}
            playerImg={queen_white_img}
            onSelectGameType={this.onSelectGameType}
          />
          <GameType
            gameType={'Single'}
            color={BLACK}
            playerImg={queen_black_img}
            onSelectGameType={this.onSelectGameType}
          />
          <GameType
            gameType={'simulation'}
            color={null}
            playerImg={cpu_img}
            onSelectGameType={this.onSelectGameType}
          />
          <GameType
            gameType={MULTI}
            color={null}
            playerImg={cpu_img}
            onSelectGameType={this.onSelectGameType}
          />
        </div>
      )
    }
  }
}

export default App;
