import React, {Component} from 'react';
import axios from 'axios'
import {
  KNIGHT,
  QUEEN,
  ROOK,
  BISHOP,
  KING,
  PAWN,
  BLACK,
  WHITE,
  RED,
  SELECT_TILE,
  MULTI
} from './resources/constants.js';
import ChessTile from './ChessTile'
import util from './Util.js';
import Moves from './Moves.js';
import Check from './Check';
import TileFinders from './TileFinders'
import actions from './ChessBoardActions.js'
import './App.css';

const cpuUrl = 'http://127.0.0.1:5000/refactor'


class ChessBoard extends Component {

  state = this.props.store.getState();

  componentDidMount = () => {
    this.props.store.subscribe(() => this.forceUpdate());
    this.props.store.dispatch(
      actions.selectGameType({
        gameType: this.props.gameType,
        selectedColor: this.props.selectedColor
      })
    )
  }

  moveComputerPlayer = (sourceTile, destinationTile) => {
    setTimeout(()=>{this.onSelectTile(sourceTile.key)}, 1500)
    setTimeout(()=>{this.onSelectTile(destinationTile.key)}, 2000)
  }

  componentDidUpdate = (prevProps, prevState) => {
    let activeColor = this.props.store.getState().activeColor;
    let turnNbr = this.props.store.getState().turnNbr;
    let gameType = this.props.store.getState().gameType;
    let selectedColor = this.props.store.getState().selectedColor;
    let cpuColor = this.props.store.getState().cpuColor;
    let moveIsPending = this.props.store.getState().moveIsPending;
    let isCpuTurnActive = this.props.store.getState().isCpuTurnActive;

    if (gameType != MULTI) {
      if (isCpuTurnActive && !moveIsPending) {
        axios.post(cpuUrl, {
          "tiles": this.props.store.getState().tiles,
          "color": this.props.store.getState().activeColor,
          "turnNbr": this.props.store.getState().turnNbr
        }).then((response) => {
          this.moveComputerPlayer(response.data.tile_to_move, response.data.best_move);
        });
      }
    }
  }

  getMovesForPlayer = (selectedTile, tiles, activeColor, occupant) => {
    let updatedTiles = [];
    let column = selectedTile.column;
    let row = selectedTile.row;

    let tilesInSelectedRow = TileFinders.getTilesInRow(selectedTile, tiles);
    let tilesInSelectedCol = TileFinders.getTilesInColumn(selectedTile, tiles);
    let tilesInSelectedDiag = TileFinders.getTilesInDiag(selectedTile, tiles);

    switch (occupant.type) {
      case KNIGHT:
        return updatedTiles = tiles.map((tile) => {
          return Moves.knight(tile, selectedTile, tiles, occupant)
        });
      case KING:
        return updatedTiles = tiles.map((tile) => {
          return Moves.king(tile, selectedTile, tiles, occupant)
        });
      case QUEEN:
        return updatedTiles = tiles.map((tile) => {
          return Moves.queen(tile, selectedTile, tiles, occupant, tilesInSelectedRow, tilesInSelectedCol, tilesInSelectedDiag)
        });
      case ROOK:
        return updatedTiles = tiles.map((tile) => {
          return Moves.rook(tile, selectedTile, tiles, occupant, tilesInSelectedRow, tilesInSelectedCol)
        });
      case BISHOP:
        return updatedTiles = tiles.map((tile) => {
          return Moves.bishop(tile, selectedTile, tiles, occupant, tilesInSelectedDiag)
        });
      case PAWN:
        return updatedTiles = tiles.map((tile) => {
          return Moves.pawn(tile, selectedTile, tiles, occupant, tilesInSelectedCol)
        });
    }
  }

  getPossibleMoves = (selectedTile, tiles, activeColor, occupant) => {
    let updatedTiles = [];

    if (occupant && occupant.color === activeColor) {
      updatedTiles = this.getMovesForPlayer(selectedTile, tiles, activeColor, occupant);
      updatedTiles = this.removeMovesLeadingToCheck(selectedTile, selectedTile.occupant.color, updatedTiles);
    } else {
      updatedTiles = tiles.map((tile) => {
        if (tile.key === selectedTile.key) {
          tile.selected = true;
          tile.isLegalMove = false;
        } else {
          tile.selected = false;
          tile.isLegalMove = false;
        }
        return tile;
      });
    }

    return updatedTiles;
  }

  movePlayer = (prevSelectedTile, selectedTile, tiles) => {
    let occupant = prevSelectedTile.occupant;

    return tiles.map((tile) => {
      if (tile.key === selectedTile.key) {
        tile.occupant = occupant;
        tile.selected = false;
        tile.isLegalMove = false;
      } else if (tile.key === prevSelectedTile.key) {
        tile.selected = false;
        tile.isLegalMove = false;
        tile.inStartingPos = false;
        tile.occupant = null;
      } else {
        tile.selected = false;
        tile.isLegalMove = false;
      }
      return tile;
    })
  }

  removeMovesLeadingToCheck = (selectedTile, color, tiles) => {
    let player = selectedTile.occupant;
    let tmpTiles = [];

    return tiles.map((tile) => {
      if (tile.isLegalMove) {
        tmpTiles = this.movePlayer(selectedTile, tile, util.deepCopyArray(tiles));
        tile.isLegalMove = !Check.detectCheck(tmpTiles, color);
      }
      return tile;
    });
  }

  markKingAsChecked = (updatedTiles, activeColor) => {
    return updatedTiles.map((tile) => {
      if (tile.occupant && tile.occupant.type === KING && tile.occupant.color === activeColor) {
        tile.occupant.checked = true;
      }
      return tile;
    });
  }

  resolveCheck = (updatedTiles, activeColor) => {
    return updatedTiles.map((tile) => {
      if (tile.occupant && tile.occupant.type === KING && tile.occupant.color === activeColor) {
        tile.occupant.checked = false;
      }
      return tile;
    });
  }

  onSelectTile = (id) => {
    let state = this.props.store.getState()
    let moveIsPending = false;
    let possibleMoves = [];
    let updatedTiles = [];
    let enemyIsChecked = false;
    let prevSelectedTile = state.selectedTile;
    let activeColor = state.activeColor;
    let enemyColor = activeColor === BLACK ? WHITE : BLACK;
    let turnNbr = state.turnNbr;

    let captured = util.deepCopyArray(state.captured);
    let currentTiles = util.deepCopyArray(state.tiles);
    let selectedTile = this.findTileById(state.tiles, id);

    if (selectedTile.isLegalMove) {
      if (selectedTile.occupant) {
        captured.push(selectedTile.occupant);
      }
      updatedTiles = this.movePlayer(prevSelectedTile, selectedTile, currentTiles);
      updatedTiles = this.resolveCheck(updatedTiles, activeColor);
      enemyIsChecked = Check.detectCheck(updatedTiles, enemyColor);
      updatedTiles = enemyIsChecked ? this.markKingAsChecked(util.deepCopyArray(updatedTiles), enemyColor) : updatedTiles;
      activeColor = activeColor === BLACK ? WHITE : BLACK;
      turnNbr += 1;
      moveIsPending = false;
    } else {
      updatedTiles = this.getPossibleMoves(selectedTile, util.deepCopyArray(currentTiles), activeColor, selectedTile.occupant);
      moveIsPending = true;
    }

    this.props.store.dispatch(
      actions.addUpdatedTiles({
        tiles: updatedTiles,
        selectedTile: selectedTile,
        activeColor: activeColor,
        captured: captured,
        turnNbr: turnNbr,
        moveIsPending: moveIsPending
      })
    );
  }

  findTileById = (tiles, id) => {
    return tiles.find((tile) => {
      return tile.key === id;
    });
  }

  render() {
    const tiles = this.props.store.getState().tiles;

    let currRow = 1;
    let evenColor = BLACK;
    let oddColor = WHITE;

    return (
      tiles.map((tile) => {
        if (currRow != tile.row) {
          currRow = tile.row;
          evenColor = evenColor === WHITE ? BLACK : WHITE;
          oddColor = oddColor === BLACK ? WHITE : BLACK;
          return (
            <span>
              <br/>
              <ChessTile
                id={tile.key}
                row={tile.row}
                column={tile.column}
                onClick={this.onSelectTile}
                selected={tile.selected}
                islegalMove={tile.isLegalMove}
                occupant={tile.occupant}
                color={tile.column % 2 === 0 ? evenColor : oddColor}
                checked={tile.occupant ? tile.occupant.checked : false}
              />
            </span>
          )
        } else {
          return (
            <span>
              <ChessTile
                id={tile.key}
                row={tile.row}
                column={tile.column}
                onClick={this.onSelectTile}
                selected={tile.selected}
                islegalMove={tile.isLegalMove}
                occupant={tile.occupant}
                color={tile.column % 2 === 0 ? evenColor : oddColor}
                checked={tile.occupant ? tile.occupant.checked : false}
              />
            </span>
          )
        }
      })
    )
  }
}

export default ChessBoard;
