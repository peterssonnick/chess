import React, {Component} from 'react';
import {
  BLACK,
  WHITE,
  BLACK_CSS,
  WHITE_CSS,

  RED
} from './resources/constants.js';
import './App.css';

class ChessTile extends Component {

  render() {
    let displayColor = this.props.color === WHITE ? WHITE_CSS : BLACK_CSS;
    return (
      <span
        onClick={() => { this.props.onClick(this.props.id) }}
        className={this.props.selected || this.props.islegalMove ? "selectedTile" : "tile"}
        style={{
          backgroundColor: this.props.checked ? RED : displayColor
        }}
      >
        <img
          src={this.props.occupant ? this.props.occupant.img : ''}
          className={'occupant'}
          style={{
            visibility: this.props.occupant ? 'visible' : 'hidden',
          }}
        />
      </span>
    )
  }
}

export default ChessTile;
