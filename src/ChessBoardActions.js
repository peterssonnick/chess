import {
  KNIGHT,
  QUEEN,
  ROOK,
  BISHOP,
  KING,
  PAWN,
  BLACK,
  WHITE,
  RED,
  SELECT_TILE,
  SELECT_GAME_TYPE
} from './resources/constants.js';

function addUpdatedTiles(updatedTileObj) {
  return {
    type: SELECT_TILE,
    tiles: updatedTileObj.tiles,
    selectedTile: updatedTileObj.selectedTile,
    activeColor: updatedTileObj.activeColor,
    captured: updatedTileObj.captured,
    turnNbr: updatedTileObj.turnNbr,
    moveIsPending: updatedTileObj.moveIsPending
  }
}

function selectGameType(gameDetail) {
  return {
    type: SELECT_GAME_TYPE,
    gameType: gameDetail.gameType,
    selectedColor: gameDetail.selectedColor,
    cpuColor: gameDetail.selectedColor == WHITE ? BLACK : WHITE
  }
}

const actions = {
  addUpdatedTiles,
  selectGameType
}

export default actions;
