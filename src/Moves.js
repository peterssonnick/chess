let isYPathClear = (selectedCol, selectedRow, targetRow, colTiles, tiles) => {
  let yPathIsClear = true;

  colTiles.map((tile) => {
    if (tile.row > selectedRow && tile.row < targetRow || tile.row < selectedRow && tile.row > targetRow) {
      if (tile.occupant) {
        yPathIsClear = false;
      }
    }
    return tile;
  });

  return yPathIsClear;
}

let isXPathClear = (selectedRow, selectedCol, targetCol, rowTiles, tiles) => {
  let xPathIsClear = true;

  rowTiles.map((tile) => {
    if (tile.column > selectedCol && tile.column < targetCol || tile.column < selectedCol && tile.column > targetCol) {
      if (tile.occupant) {
        xPathIsClear = false;
      }
    }
    return tile;
  });

  return xPathIsClear;
}

let isDiagPathClear = (selectedRow, selectedCol, targetRow, targetCol, diagTiles, tiles) => {
  let diagPathIsClear = true;

  diagTiles.map((tile) => {
    if (Math.abs(tile.column - targetCol) === Math.abs(tile.row - targetRow)
          && (tile.column > targetCol && tile.column < selectedCol
          || tile.column < targetCol && tile.column > selectedCol)) {
      if (tile.occupant) {
        diagPathIsClear = false;
      }
    }
  });

  return diagPathIsClear;
}

let isPawnMoveInMaxY = (selectedRow, targetRow, maxYAdvance) => {
  let isLegalMove = true;

  if (maxYAdvance > 0) {
    isLegalMove = targetRow - selectedRow <= maxYAdvance && targetRow > selectedRow;
  } else {
    isLegalMove = targetRow - selectedRow >= maxYAdvance && targetRow < selectedRow;
  }

  return isLegalMove;
}

let isOccupantEnemy = (targetTile, selectedOccupantColor) => {
  return targetTile.occupant.color !== selectedOccupantColor;
}

let knight = (tile, selectedTile, tiles, occupant) => {
  let column = selectedTile.column;
  let row = selectedTile.row;
  let key = selectedTile.key;
  let color = occupant.color;

  if (tile.row === row + 2 && tile.column === column + 1) {
    tile.isLegalMove = tile.occupant ? isOccupantEnemy(tile, color) : true;
  } else if (tile.row === row + 2 && tile.column === column - 1) {
    tile.isLegalMove = tile.occupant ? isOccupantEnemy(tile, color) : true;
  } else if (tile.row === row + 1 && tile.column === column + 2) {
    tile.isLegalMove = tile.occupant ? isOccupantEnemy(tile, color) : true;
  } else if (tile.row === row + 1 && tile.column === column - 2) {
    tile.isLegalMove = tile.occupant ? isOccupantEnemy(tile, color) : true;
  } else if (tile.row === row - 2 && tile.column === column + 1) {
    tile.isLegalMove = tile.occupant ? isOccupantEnemy(tile, color) : true;
  } else if (tile.row === row - 2 && tile.column === column - 1) {
    tile.isLegalMove = tile.occupant ? isOccupantEnemy(tile, color) : true;
  } else if (tile.row === row - 1 && tile.column === column + 2) {
    tile.isLegalMove = tile.occupant ? isOccupantEnemy(tile, color) : true;
  } else if (tile.row === row - 1 && tile.column === column - 2) {
    tile.isLegalMove = tile.occupant ? isOccupantEnemy(tile, color) : true;
  } else if (tile.key === key) {
    tile.selected = true;
    tile.isLegalMove = false;
  } else {
    tile.selected = false;
    tile.isLegalMove = false;
  }
  return tile;
}

let queen = (tile, selectedTile, tiles, occupant, tilesInSelectedRow, tilesInSelectedCol, tilesInSelectedDiag) => {
  let column = selectedTile.column;
  let row = selectedTile.row;
  let key = selectedTile.key;
  let color = occupant.color;

  if (tile.key === key) {
    tile.selected = true;
    tile.isLegalMove = false;
  } else if (tile.row === row && isXPathClear(row, column, tile.column, tilesInSelectedRow, tiles)) {
    tile.isLegalMove = tile.occupant ? isOccupantEnemy(tile, color) : true;
  } else if (tile.column === column && isYPathClear(column, row, tile.row, tilesInSelectedCol, tiles)) {
    tile.isLegalMove = tile.occupant ? isOccupantEnemy(tile, color) : true;
  } else if (Math.abs(tile.column - column) === Math.abs(tile.row - row)
            && isDiagPathClear(row, column, tile.row, tile.column, tilesInSelectedDiag, tiles)) {
    tile.isLegalMove = tile.occupant ? isOccupantEnemy(tile, color) : true;
  } else {
    tile.selected = false;
    tile.isLegalMove = false;
  }
  return tile;
}

let bishop = (tile, selectedTile, tiles, occupant, tilesInSelectedDiag) => {
  let column = selectedTile.column;
  let row = selectedTile.row;
  let key = selectedTile.key;
  let color = occupant.color;

  if (tile.key === key) {
    tile.selected = true;
    tile.isLegalMove = false;
  } else if (Math.abs(tile.column - column) === Math.abs(tile.row - row)
            && isDiagPathClear(row, column, tile.row, tile.column, tilesInSelectedDiag, tiles)) {
    tile.isLegalMove = tile.occupant ? isOccupantEnemy(tile, color) : true;
  } else {
    tile.selected = false;
    tile.isLegalMove = false;
  }
  return tile;
}

let rook = (tile, selectedTile, tiles, occupant, tilesInSelectedRow, tilesInSelectedCol) => {
  let column = selectedTile.column;
  let row = selectedTile.row;
  let key = selectedTile.key;
  let color = occupant.color;

  if (tile.key === key) {
    tile.selected = true;
    tile.isLegalMove = false;
  } else if (tile.row === row && isXPathClear(row, column, tile.column, tilesInSelectedRow, tiles)) {
    tile.isLegalMove = tile.occupant ? isOccupantEnemy(tile, color) : true;
  } else if (tile.column === column && isYPathClear(column, row, tile.row, tilesInSelectedCol, tiles)) {
    tile.isLegalMove = tile.occupant ? isOccupantEnemy(tile, color) : true;
  } else {
    tile.selected = false;
    tile.isLegalMove = false;
  }

  return tile;
}

let king = (tile, selectedTile, tiles, occupant) => {
  let column = selectedTile.column;
  let row = selectedTile.row;
  let key = selectedTile.key;
  let color = occupant.color;

  if (tile.key === key) {
    tile.selected = true;
    tile.isLegalMove = false;
  } else if (Math.abs(tile.row - row) <= 1 && Math.abs(tile.column - column) <= 1) {
    tile.isLegalMove = tile.occupant ? isOccupantEnemy(tile, color) : true;
  } else {
    tile.selected = false;
    tile.isLegalMove = false;
  }

  return tile;
}

let pawn = (tile, selectedTile, tiles, occupant, tilesInSelectedCol) => {
  let column = selectedTile.column;
  let row = selectedTile.row;
  let key = selectedTile.key;
  let color = occupant.color;

  let yDir = occupant.yDir;
  let maxYAdvance = selectedTile.inStartingPos ? (yDir * 2) : (yDir * 1);

  if (tile.key === key) {
    tile.selected = true;
    tile.isLegalMove = false;
  } else if (tile.column === column && isPawnMoveInMaxY(row, tile.row, maxYAdvance)) {
    tile.isLegalMove = !tile.occupant && isYPathClear(column, row, tile.row, tilesInSelectedCol, tiles);
  } else if (tile.occupant
      && (tile.row - row === yDir)
      && Math.abs(tile.row - row) === 1
      && Math.abs(tile.column - column) === 1) {
    tile.isLegalMove = isOccupantEnemy(tile, color);
  } else {
    tile.selected = false;
    tile.isLegalMove = false;
  }

  return tile;
}

const Moves = {
  isYPathClear,
  isXPathClear,
  isDiagPathClear,
  isOccupantEnemy,
  isPawnMoveInMaxY,
  queen,
  knight,
  king,
  bishop,
  rook,
  pawn
}

export default Moves;
