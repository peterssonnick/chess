import TileFinders from './TileFinders'
import {
  KNIGHT,
  QUEEN,
  ROOK,
  BISHOP,
  KING,
  PAWN,
  BLACK,
  WHITE,
  RED
} from './resources/constants.js';

let detectCheck = (tiles, detectColor) => {
  let isCheckedX = false;
  let isCheckedY = false;
  let isCheckedDiag = false;
  let isCheckedKnight = false;
  let king = TileFinders.findMyKing(tiles, detectColor);

  isCheckedX = checkThreatX(king, tiles);
  isCheckedY = checkThreatY(king, tiles);
  isCheckedDiag = checkThreatDiag(king, tiles);
  isCheckedKnight = checkThreatKnight(king, tiles);

  return isCheckedX || isCheckedY || isCheckedDiag || isCheckedKnight;
}

let checkThreatX = (kingTile, tiles) => {
  let closestLeftDiff = 10;
  let closestRightDiff = 10;
  let closestLeftTile = null;
  let closestRightTile = null;
  let threatX = false;
  let xTiles = [];
  let xThreats = [QUEEN, ROOK]

  xTiles = TileFinders.getTilesInRow(kingTile, tiles);

  for (let i = 0; i < xTiles.length; i++) {
    let tile = xTiles[i];

    if (tile.occupant) {
      if (tile.column < kingTile.column) {
        if (Math.abs(tile.column - kingTile.column) < closestLeftDiff) {
          closestLeftTile = tile;
          closestLeftDiff = Math.abs(tile.column - kingTile.column);
        }
      } else if (tile.column > kingTile.column) {
        if (tile.column - kingTile.column < closestRightDiff) {
          closestRightTile = tile;
          closestRightDiff = tile.column - kingTile.column;
        }
      }
    }
  }

  let threatLeft = assessThreats(closestLeftTile, kingTile, xThreats);
  let threatRight = assessThreats(closestRightTile, kingTile, xThreats);

  return threatLeft || threatRight;
}

let checkThreatY = (kingTile, tiles) => {
  let closestUpDiff = 10;
  let closestDownDiff = 10;
  let closestUpTile = null;
  let closestDownTile = null;
  let threatY = false;
  let yTiles = [];
  let yThreats = [QUEEN, ROOK]

  yTiles = TileFinders.getTilesInColumn(kingTile, tiles);

  for (let i = 0; i < yTiles.length; i++) {
    let tile = yTiles[i];

    if (tile.occupant) {
      if (tile.row < kingTile.row) {
        if (kingTile.row - tile.row < closestUpDiff) {
          closestUpTile = tile;
          closestUpDiff = kingTile.row - tile.row;
        }
      } else if (tile.row > kingTile.row) {
        if (tile.row - kingTile.row < closestDownDiff) {
          closestDownTile = tile;
          closestDownDiff = tile.row - kingTile.row;
        }
      }
    }
  }

  let threatUp = assessThreats(closestUpTile, kingTile, yThreats);
  let threatDown = assessThreats(closestDownTile, kingTile, yThreats);

  return threatUp || threatDown;
}

let checkThreatDiag = (kingTile, tiles) => {
  let closestUpLeftDiff = 10;
  let closestUpRightDiff = 10;
  let closestDownLeftDiff = 10;
  let closestDownRightDiff = 10;

  let closestUpLeftTile = null;
  let closestUpRightTile = null;
  let closestDownLeftTile = null;
  let closestDownRightTile = null;

  let threatDiag = false;
  let diagTiles = [];
  let diagThreats = [QUEEN, BISHOP, PAWN]

  let kingRow = kingTile.row;
  let kingCol = kingTile.column;
  let diagCol = null;
  let diagRow = null;

  diagTiles = TileFinders.getTilesInDiag(kingTile, tiles);

  for (let i = 0; i < diagTiles.length; i++) {
    let tile = diagTiles[i];
    diagCol = tile.column;
    diagRow = tile.row;

    if (tile.occupant) {
        //upLeft
      if (diagCol < kingCol && diagRow > kingRow) {
        if (kingCol - diagCol < closestUpLeftDiff) {
          closestUpLeftDiff = kingCol - diagCol;
          closestUpLeftTile = tile;
        }
        //upRight
      } else if (diagCol > kingCol && diagRow > kingRow) {
        if (diagCol - kingCol < closestUpRightDiff) {
          closestUpRightDiff = diagCol - kingCol;
          closestUpRightTile = tile;
        }
        //downLeft
      } else if (diagCol < kingCol && diagRow < kingRow) {
        if (kingCol - diagCol < closestDownLeftDiff) {
          closestDownLeftDiff = kingCol - diagCol;
          closestDownLeftTile = tile;
        }
        //downRight
      } else if (diagCol > kingCol && diagRow < kingRow) {
        if (diagCol - kingCol < closestDownRightDiff) {
          closestDownRightDiff = diagCol - kingCol;
          closestDownRightTile = tile;
        }
      }
    }
  }

  let threatUpLeft = assessThreats(closestUpLeftTile, kingTile, diagThreats);
  let threatUpRight = assessThreats(closestUpRightTile, kingTile, diagThreats);
  let threatDownLeft = assessThreats(closestDownLeftTile, kingTile, diagThreats);
  let threatDownRight = assessThreats(closestDownRightTile, kingTile, diagThreats);

  return threatUpLeft || threatUpRight || threatDownLeft || threatDownRight;
}

let checkThreatKnight = (kingTile, tiles) => {
  let isKnightThreat = false;
  let knightTiles = TileFinders.getTilesInKnightJump(kingTile, tiles);

  for (let i = 0; i < knightTiles.length; i++) {
    if (assessThreats(knightTiles[i], kingTile, [KNIGHT])) {
      isKnightThreat = true;
    }
  }

  return isKnightThreat;
}

let assessThreats = (closestThreatTile, kingTile, threatTypes) => {
  let threatIsReal = false;

  if (closestThreatTile && closestThreatTile.occupant) {
    switch (closestThreatTile.occupant.type) {
      case PAWN:
        threatIsReal = isPawnThreatReal(closestThreatTile, kingTile);
        break;
      case KING:
        threatIsReal = isKingThreatReal(closestThreatTile, kingTile);
        break;
      default:
        threatIsReal = isThreatReal(closestThreatTile, kingTile, threatTypes);
        break;
    }
  }

  return threatIsReal;
}

let isPawnThreatReal = (closestThreatTile, kingTile) => {
  let isPawnThreatReal = false;

  if (closestThreatTile && closestThreatTile.occupant && closestThreatTile.occupant.type === PAWN) {
    if (closestThreatTile.occupant.yDir === (kingTile.row - closestThreatTile.row)) {
      if (Math.abs(closestThreatTile.column - kingTile.column) == 1) {
        if (closestThreatTile.occupant.color != kingTile.occupant.color) {
          isPawnThreatReal = true;
        }  
      }
    }
  }

  return isPawnThreatReal;
}

let isKingThreatReal = (closestThreatTile, kingTile) => {
  let isKingThreatReal = false;

  if (closestThreatTile && closestThreatTile.occupant && closestThreatTile.occupant.type === KING) {
    if (Math.abs(kingTile.row - closestThreatTile.row) === 1 && Math.abs(kingTile.column - closestThreatTile.column) === 1) {
      if (closestThreatTile.occupant.color != kingTile.occupant.color) {
        isKingThreatReal = true;
      }
    }
  }

  return isKingThreatReal;
}

let isThreatReal = (closestThreatTile, kingTile, threatTypes) => {
  let isThreatReal = false;

  if (closestThreatTile && closestThreatTile.occupant) {
    if (closestThreatTile.occupant.color != kingTile.occupant.color) {
      if (threatTypes.includes(closestThreatTile.occupant.type)) {
        isThreatReal = true;
      }
    }
  }

  return isThreatReal;
}

const Check = {
  detectCheck,
  isThreatReal,
  isKingThreatReal,
  isPawnThreatReal,
  assessThreats,
  checkThreatX,
  checkThreatY,
  checkThreatDiag,
  checkThreatKnight
};

export default Check;
